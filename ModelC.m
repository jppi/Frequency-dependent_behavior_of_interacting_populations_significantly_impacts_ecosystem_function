clear all
% Initial conditions
Parameters;

    %Replicator parameters
dNmax0 = 10^-11;
dPmax0 = 10^-11;
dtfact = 0.1; %how much change in frequencies at each time step of the replicator

    % Temporal parameters
T = 5000; % [-] Duration of the simulation
Tvect = [0:1:1000,1001:0.1:1500]; % [days] times of each run - cut in two to have more precision towards the end
DTvect = diff(Tvect); %diff(Tvect); % [days] time step between each run
Iavg = 5000; %how many of last time points do we want to store
lambda = 0.5; % [-] Max prop of decrease of the population biomass at each time step


Ks = linspace(1,4,40); % surface carrying capacity is 10^Ks - max clearance rate of predator is 10^bmax


    %Variables to save the outputs
FitN = zeros(size(Ks));
FitP = FitN;
FitiniN = FitN; FitiniF = FitN;
NSav = zeros(2*P.n, Iavg, size(Ks,2)); %Positions of prey at each time step for each run
PSav = zeros(2*P.n, Iavg, size(Ks,2)); %Positions of predator at each time step for each run
BioSav = zeros(2+P.n, Iavg, size(Ks,2)); %Biomass for prey / predator / ressource at each time step for each run

Biot = zeros(P.n+2,1); %dummy to use when updating biomass
TTEsav = zeros(Iavg, size(Ks,2));


%Useful functions for the growth
 l = @(z) exp(-P.klight*z); % functional form of light
 gfun = @(z) (1 - tanh((-P.zo + z)/P.zm))/2; % functional form of growth
 bfun = @(z) P.Lmax*l(z)./(P.L0 + P.Lmax*l(z)); % functional form of clearance rate
 ofun = @(z) P.attenuation*P.Lmax*l(z)./(P.L0 + P.attenuation*P.Lmax*l(z)); %functional form of clearance rate at night
Vi = (1:P.n); %Water layers
Ai = Vi'*ones(1,P.n);

CN = P.cn*P.dZ*abs(Ai-Ai');
CP = P.cf*P.dZ*abs(Ai-Ai');

                    CONT = zeros(size(Ks,2),1);

         % Initial biomasses
        P.K = 0.01*max(10^Ks(end)* (1 - tanh((-P.zo + P.zi)/P.zm))/2, 10^-6);
        P0 = [0.1, 0.005, P.K]; % [m^-3] Let's assume they are all at their carrying capacity at the beginning
        Bio = P0;  
%     Tvect =  [0:0.1:40000];
%    load December10.mat
   Tvect = [0:1:500,501:0.1:1000]; 
for ii=1:size(Ks,2) %16
    ii
    P.Ks = 10^(Ks(ii));
    P.K = max(P.Ks * (1 - tanh((-P.zo + P.zi)/P.zm))/2, 10^-6); % [m^-3] Depth-dependent carrying capacity of the resource
    
      if ii==1 %we initialize only at the first run, otherwise we continue with the distribution of the previous run
        N = rand(P.n)/sum(sum(ones(P.n))); %initialization for the replicator
        F = rand(P.n)/sum(sum(ones(P.n)));
                
     end

   
    v = P.bmax*bfun(P.zi); %[m3 day^-1] Day clearance of predators
    o = P.bmax*ofun(P.zi); %[m3 day^-1] Night clearance of predators
   

for t=1:size(DTvect,2)-1
    DT = DTvect(t);
    
     g = P.gmax*Bio(3:end); %[day^-1] Growth rate of prey
     GN = (1-P.a)*g'*ones(1,P.n) + P.a*ones(P.n,1)*g - CN ;
     
     
    Ns = 0; Fs = 0; %just for the initialization of the while loop
    cont = 0; 
     
    Nday = sum(N,1);
    Nnig = sum(N,2);
    Fday = sum(F,1);
    Fnig = sum(F,2);
    
    while cont < 10^5 && ( sum(sum(abs(Ns - N)))/sum(sum(N)) > 1e-6  || sum(sum(abs(Fs - F)))/sum(sum(F)) > 1e-6 ) %<10^5 and 1e-4 and 1e-4 will fasten the process by maybe 10
        cont = cont+1;
        Ns = N; Fs = F;
    
        MN = P.n*Bio(2)*((1-P.a)*(o'.*Fnig)*ones(1,P.n) + P.a*ones(P.n,1)*(Fday.*v)) + P.bg ; %[day^-1]
        GP = P.epsilon*P.n*Bio(1)*((1-P.a)*(o'.*Nnig)*ones(1,P.n) + P.a*ones(P.n,1)*(Nday.*v)) - CP; %[day^-1]
        MP = P.m*P.n*Bio(2)*((1-P.a)*Fnig*ones(1,P.n) + P.a*ones(P.n,1)*Fday+0.1); %[day^-1]
        FN = GN - MN; %[day^-1]
        FP = GP - MP; %[day^-1]
    
        FNmax = max(max(FN)); FNmin = min(min(FN));
        FPmax = max(max(FP)); FPmin = min(min(FP));
        fact = dtfact/max([FPmax,FNmax,-FNmin,-FPmin]); % fact is dynamic so that maximum increase is at most 50% per time step
    
        N = N + fact*FN.*N;
        F = F + fact*FP.*F;  
    
        N(N<dNmax0)=dNmax0; F(F<dPmax0)=dPmax0;
        
        N = N/sum(sum(N));  F = F/sum(sum(F));
        Nday = sum(N,1); Nnig = sum(N,2);
        Fday = sum(F,1); Fnig = sum(F,2);  
                  
    end
    
    Biot(1) =  max(min(Bio(1)*(1+sum(sum(FN.*N))*DT), 2*Bio(1)), 0.5*Bio(1));
    Biot(2) =  max(min(Bio(2)*(1+sum(sum(FP.*F))*DT), 2*Bio(2)), 0.5*Bio(2)); 
    Biot(3:end) = max(min(Bio(3:end) + (P.r.*(1-Bio(3:end)./P.K) -...
                  (P.a*Nday*P.n.*Bio(1).*P.gmax + (1-P.a)*Nnig'*P.n.*Bio(1).*P.gmax)).*Bio(3:end)*DT,...
                  2*Bio(3:end)),0.5*Bio(3:end)); 
    Bio = Biot';
    
    
    if  t > size(DTvect,2) -1 - Iavg
        NSav(:,t - size(DTvect,2) + 1 + Iavg,ii) = [Nday'; Nnig];
        PSav(:,t - size(DTvect,2) + 1 + Iavg,ii) = [Fday'; Fnig];
        BioSav(:,t - size(DTvect,2) + 1 + Iavg,ii) = Biot;
        
        tte = 1/3*(1-(0.01+CN)./(GN+CN)).*N;
        TTE = sum(sum(tte(N>10^-5)));
        
        TTEsav(t - size(DTvect,2) + 1 + Iavg,ii) = TTE;
    end
    
end
                    CONT(ii) = cont;
FitN(ii) = FNmax;
FitP(ii) = FPmax;

     
ii
%  Plot_temporal_1run; %if we want to see the temporal evolution of this run
   save ModelC.mat -v7.3
end


   plot_total_K;
  %Plot_temporal_1run