klight = 0.07; % [m^-1] light attenuation coefficient
% r0 = 1; % [day^-1] Maximum growth rate of the resource


Ks = linspace(0,5,100);

% K0 = 10^2; % [m^-3] Carrying capacity of phytoplankton at the surface
z0 = 50; % [m] Mixed layer depth
zs = 10; % [m] Sharpness of the transition zone to depleted depths
Kt = @(k,z) max(k/2*(1-tanh((z-z0)/zs)),10^-6); % [m^-3] Carrying capacity of phytoplankton

bmax = 0.1; %0.0042;%025;
g = 0.001;%0.005;%3.5e-4;%0.005; % [m^3 day^-1] Clearance rate of zooplankton
%mu = @(z) bmax.*exp(-klight*z)*0.1 + 0.01; % [day^-1] mortality = average clearance rate predator * approx. predator concentration in the WC  

zz = 0:300;
Res = zeros(size(zz,2),size(Ks,2));
Zoo = Res;
Fish = Res;
TTE_theo = zeros(1,size(Ks,2));
TTE_surf = TTE_theo;


for jj=1:size(Ks,2)
   for ii=1:size(zz,2)
    K = Kt(10^Ks(jj),zz(ii));
    r = 1; %0.017;
    
    m0 = bmax*exp(-klight*zz(ii));
    mu = 0.01; %2.6e-5; %0.01;
    mu0 = 10^-3; %10^-3;
    mu1 =1e-3; %0.067;% 5*10^-3;
     

    N = r*(K/3*g+m0*mu1/mu0-mu)*mu0/(g*K*mu0/3*g+r/3*m0^2);%r*(1+1/K*(m0*mu1/mu0-mu)*3/g)/(g+m0^2/K/3/mu0*3/g);

    F = (-mu1+m0/3*N)/mu0;

    Phi = (m0*F+mu)/(g/3);
    
    if F < 0 || N < 0
        if N > 0
            F = 0;
            Phi = min(K,mu*3 / g);
            N = r*(1-Phi/K)/g;
        else
            F = 0;
            N = 0;
            Phi = K;
        end
    end
        
    Res(ii,jj) = Phi;
    Zoo(ii,jj) = N;
    Fish(ii,jj) = F;
   end
    
    TTE_theo(jj) = sum(1/3*(bmax*exp(-klight*zz)'.*Fish(:,jj)./(bmax*exp(-klight*zz)'.*Fish(:,jj)+mu)).*Zoo(:,jj))/sum(Zoo(:,jj));
    TTE_surf(jj) = 1/3*bmax*exp(-klight*zz(1))'.*Fish(1,jj)./(bmax*exp(-klight*zz(1))'.*Fish(1,jj)+mu);
end
%  Res = Res*0.01;
 
Phi = Res;
mZ = Zoo;
 
% figure
% subplot(131)
% imagesc(Ks,zz,log10(Res))
% caxis([0 100])
% colormap(flipud(hot))
% colorbar
% subplot(132)
%   imagesc(Ks,zz,max(0,log10(Zoo)))  
%   colormap(flipud(hot))
% colorbar
% subplot(133)
% imagesc(Ks,zz,max(0,log10(Fish)))
% colormap(flipud(hot))
% colorbar
%    caxis([0 500])
   
%  save theoretical.mat

 figure
subplot(221)
semilogy(Ks, Res(1,:))
title('Surface phytoplankton concentration')
subplot(222)
semilogy(Ks,-Res(1,:)+Kt(10.^Ks,zz(1)))
xlim([Ks(1) Ks(end)])
title('Difference carrying capacity - phyto')
subplot(223)
semilogy(Ks, sum(Zoo(:,:))/size(zz,2))
xlim([Ks(1) Ks(end)])
title('Zooplankton concentration')
subplot(224)
semilogy(Ks, sum(Fish(:,:))/size(zz,2))
title('Fish concentration')
xlim([Ks(1) Ks(end)])

figure
plot(Ks,TTE_theo)