iavg = 1000; % from which time step to we start for the mean and error bars 
 load December10.mat
load B.mat
figure
  subplot(2,2,1)
  imagesc(Ks,P.zi,squeeze(mean(NSav(1:P.n, iavg:end, :),2)).*(squeeze(mean(BioSav(1,iavg:end,:),2))>10^-3)', [0 0.5])
  ylabel({'Position during', 'daytime at equilibrium'})
  colormap(flipud(hot));
  freezeColors;
%   plot(1:41,depthmaxG(1,:),'--k')
  
  subplot(2,2,2)
  imagesc(Ks,P.zi,squeeze(mean(PSav(1:P.n, iavg:end, :),2)).*(squeeze(mean(BioSav(2,iavg:end,:),2))>1.1*10^-3)', [0 0.5])
  colormap(flipud(B(30:end,:)));
  caxis([0 0.3])
  freezeColors;
%   plot(1:41,depthmaxG(2,:),'--k')
  
  subplot(2,2,3)
  imagesc(Ks,P.zi,squeeze(mean(NSav(P.n+1:end, iavg:end, :),2)).*(squeeze(mean(BioSav(1,iavg:end,:),2))>10^-3)', [0 0.5])
  ylabel({'Position during', 'nighttime at equilibrium'})
  xlabel('log_1_0(K_s)')
  colormap(flipud(hot));
%    h = colorbar; %uncomment these two to see colorbars
%   cbfreeze(h);
  freezeColors;
%   plot(1:41,depthmaxG(3,:),'--k')
  
  subplot(2,2,4)
  imagesc(Ks,P.zi,squeeze(mean(PSav(P.n+1:end, iavg:end, :),2)).*(squeeze(mean(BioSav(2,iavg:end,:),2))>1.1*10^-3)', [0 0.5])
  xlabel('log_1_0(K_s)')
%    ax1 = gca;
%   cmp = colormap(ax1,flipud(B(15:end,:)));
colormap(flipud(B(30:end,:)));
caxis([0 0.3])
%    h2 = colorbar(ax1);
%   cbfreeze(h2);
    freezeColors;
%   plot(1:41,depthmaxG(4,:),'--k')
  
%   subplot(3,3,7)
%   semilogy(Ks,max(10^-5,squeeze(mean(BioSav(1,iavg:end,:),2))),'k')
%   hold on
%   ciplot(max(10^-5,squeeze(min(BioSav(1,iavg:end,:),[],2)))',max(10^-5,squeeze(max(BioSav(1,iavg:end,:),[],2)))',Ks,'k')
%   hold on
%   xlabel('Biomass of prey [m^-^3]')
%   
%   
%   subplot(3,3,8)
%  semilogy(Ks,squeeze(mean(BioSav(2,iavg:end,:),2)),'k')
%   hold on
% ciplot(max(10^-5,squeeze(min(BioSav(2,iavg:end,:),[],2)))',max(10^-5,squeeze(max(BioSav(2,iavg:end,:),[],2)))',Ks,'k')
%   
%  xlabel('Biomass of predator [m^-^3]')
%  
% subplot(339)
% imagesc(Ks,P.zi,squeeze(mean(BioSav(3:end,iavg:end,:),2)))
%    colorbar