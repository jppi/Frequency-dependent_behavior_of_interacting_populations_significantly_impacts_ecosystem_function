%%% ENVIRONMENTAL PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
P.a = 0.65; % [-] Proportion of day in 24h

P.H = 300; %[m] Depth of the water column
P.n = 30; % [-]  Number of water layers for the discretization

P.zext = linspace(0,P.H,P.n+1); % [m] different depths of the discretized water column
P.zi = (P.zext(2:end)+P.zext(1:end-1))/2; % [m] Middle position of each water layer
P.dZ = P.H/P.n; % [m] Width of each water layer

%LIGHT LEVELS
P.Lmax = 100; %[W/m^2] Maximum irradiance
P.attenuation = 10^-5; % [-] Attenuation difference between day and night
P.klight = 0.07; % [m^-1] Light attenuation coefficient


%%% BIOLOGICAL PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
P.nplayer = 2; % [-] Nb of players
%P.miniC = 10^-10; % [m^-2] Minimum concentration allowed for phytoplankton, zooplankton and fish in the system

%RESOURCES 
P.zo = 50; % [m] Mixed layer depth
P.zm = 10; % [m] Sharpness of the transition zone of the surface
P.rs = 1; % [day^-1] Growth rate of phytoplankton at the surface
% P.Ks = 100; % [m^-3] Carrying capacity for phytoplankton at the surface (can probably be a bit higher)

P.r = P.rs * exp(-P.klight * P.zi); % [day^-1] Depth-dependent potential growth rate of the resource
% P.K = max(P.Ks * (1 - tanh((-P.zo + P.zi)/P.zm))/2, 10^-6); % [m^-3] Depth-dependent carrying capacity of the resource

%ZOOPLANKTON
    %Growth
P.gmax = 0.001;%0.1/100; %1.5 * 10^-3; % [m^3 day^-1] Encounter rate of zooplankton for the resource
P.bg = 0.01; % [day^-1] Background mortality rate per day

    %Migration cost
P.cn = 10^-5; % [m^-1 day^-1] unitary migration cost for a zpk


%FISH  
    %Clearance rate
P.L0 = 1; % [W m^-2] Half-saturation light level for visual predators    
P.bmax = 0.1; % [m^3 day^-1] Maximum clearance rate for the fish
P.bd = P.Lmax * exp(-P.klight*P.zi)./(P.L0 + P.Lmax * exp(-P.klight*P.zi)); % [-] Clearance rate of fish during daytime - to multiply by bmax   
P.bn = P.attenuation * P.Lmax * exp(-P.klight*P.zi)./(P.L0 + P.attenuation * P.Lmax * exp(-P.klight*P.zi));  % [-] Clearance rate of fish during nighttime - to multiply by bmax
P.epsilon = 10^-2; % [-] Assimilation efficiency of the predator

    %Mortality
P.m = 10^-3; % [m^3 day^-1] Density dependent mortality

    %Migration cost
P.cf = 10^-5; % [m^-1 day^-1] unitary migration cost for a fish


